﻿using Emgu.CV.UI;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tesseract;

namespace IPCV_Home_Assignment
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            ImageViewer viewer = new ImageViewer();

            VideoCapture videoCapture = new VideoCapture();

            //Current stream video capture.
            Application.Idle += new EventHandler(delegate (object sender, EventArgs e)
           {
               imgCameraStream.Image = videoCapture.QueryFrame();
               imgCameraStream.SizeMode = PictureBoxSizeMode.StretchImage;
           });

            //Convert current stream to binary
            Application.Idle += new EventHandler(delegate (object sender, EventArgs e)
            {
                Image<Bgr, Byte> orginal = videoCapture.QueryFrame().ToImage<Bgr, Byte>(false);
                Image<Gray, byte> gray = orginal.Convert<Gray, byte>().PyrDown().PyrUp().ThresholdBinary(new Gray(80), new Gray(255));

                imgBinaryStream.Image = gray;
                imgBinaryStream.SizeMode = PictureBoxSizeMode.StretchImage;
               
            });

            //OCR on current stream
            Application.Idle += new EventHandler(delegate (object sender, EventArgs e)
            {
                imgOCR.Image = videoCapture.QueryFrame();
                Bitmap stream = imgOCR.Image.Bitmap;
                TesseractEngine tesseractEngine = new TesseractEngine("./tessdata","eng",EngineMode.Default);
                Page page = tesseractEngine.Process(stream, PageSegMode.AutoOsd);
                rtbOcrResults.Text = page.GetText();
                imgOCR.SizeMode = PictureBoxSizeMode.StretchImage;
            });

            //Canny Edge Detection
            Application.Idle += new EventHandler(delegate (object sender, EventArgs e)
            {

                //double cannyThresholdLinking = 120.0;
                //UMat cannyEdges = new UMat();
                //double cannyThreshold = 190;
                //imgCanny.Image = videoCapture.QueryFrame();
                //CvInvoke.Canny(imgOCR.Image, cannyEdges, cannyThreshold, cannyThresholdLinking);

                //LineSegment2D[] lines = CvInvoke.HoughLinesP(
                //    cannyEdges, 1, //Distance resolution in pixel-related units
                //    Math.PI / 60, //Angle resolution measured in radians.
                //    40, //threshold
                //    25, //min Line width
                //    10); //gap between lines

                //Image<Bgr, Byte> lineImage = videoCapture.QueryFrame().ToImage<Bgr, Byte>(false).CopyBlank();
                //foreach (LineSegment2D line in lines)
                //    lineImage.Draw(line, new Bgr(255, 255, 255), 1);
                //imgCanny.SizeMode = PictureBoxSizeMode.StretchImage;
                //imgCanny.Image = lineImage;

                Image<Bgr, Byte> orginal = videoCapture.QueryFrame().ToImage<Bgr, Byte>(false);
                Image<Gray, byte> gray = orginal.Convert<Gray, byte>().PyrDown().PyrUp();
                imgCanny.Image = gray.Canny(10,40);
                imgCanny.SizeMode = PictureBoxSizeMode.StretchImage;

            });

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}
