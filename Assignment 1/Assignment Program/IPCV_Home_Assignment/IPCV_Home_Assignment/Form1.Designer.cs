﻿namespace IPCV_Home_Assignment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imgCameraStream = new Emgu.CV.UI.ImageBox();
            this.imgBinaryStream = new Emgu.CV.UI.ImageBox();
            this.imgOCR = new Emgu.CV.UI.ImageBox();
            this.rtbOcrResults = new System.Windows.Forms.RichTextBox();
            this.imgCanny = new Emgu.CV.UI.ImageBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgCameraStream)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBinaryStream)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgOCR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCanny)).BeginInit();
            this.SuspendLayout();
            // 
            // imgCameraStream
            // 
            this.imgCameraStream.Location = new System.Drawing.Point(43, 31);
            this.imgCameraStream.Name = "imgCameraStream";
            this.imgCameraStream.Size = new System.Drawing.Size(290, 213);
            this.imgCameraStream.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgCameraStream.TabIndex = 2;
            this.imgCameraStream.TabStop = false;
            // 
            // imgBinaryStream
            // 
            this.imgBinaryStream.Location = new System.Drawing.Point(421, 31);
            this.imgBinaryStream.Name = "imgBinaryStream";
            this.imgBinaryStream.Size = new System.Drawing.Size(290, 213);
            this.imgBinaryStream.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgBinaryStream.TabIndex = 3;
            this.imgBinaryStream.TabStop = false;
            // 
            // imgOCR
            // 
            this.imgOCR.Location = new System.Drawing.Point(792, 31);
            this.imgOCR.Name = "imgOCR";
            this.imgOCR.Size = new System.Drawing.Size(290, 213);
            this.imgOCR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgOCR.TabIndex = 4;
            this.imgOCR.TabStop = false;
            // 
            // rtbOcrResults
            // 
            this.rtbOcrResults.Location = new System.Drawing.Point(1135, 31);
            this.rtbOcrResults.Name = "rtbOcrResults";
            this.rtbOcrResults.Size = new System.Drawing.Size(211, 213);
            this.rtbOcrResults.TabIndex = 5;
            this.rtbOcrResults.Text = "";
            // 
            // imgCanny
            // 
            this.imgCanny.Location = new System.Drawing.Point(43, 305);
            this.imgCanny.Name = "imgCanny";
            this.imgCanny.Size = new System.Drawing.Size(290, 213);
            this.imgCanny.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgCanny.TabIndex = 6;
            this.imgCanny.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1409, 672);
            this.Controls.Add(this.imgCanny);
            this.Controls.Add(this.rtbOcrResults);
            this.Controls.Add(this.imgOCR);
            this.Controls.Add(this.imgBinaryStream);
            this.Controls.Add(this.imgCameraStream);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imgCameraStream)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBinaryStream)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgOCR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCanny)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Emgu.CV.UI.ImageBox imgCameraStream;
        private Emgu.CV.UI.ImageBox imgBinaryStream;
        private Emgu.CV.UI.ImageBox imgOCR;
        private System.Windows.Forms.RichTextBox rtbOcrResults;
        private Emgu.CV.UI.ImageBox imgCanny;
    }
}

